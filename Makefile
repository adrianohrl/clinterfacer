clean:
	find ./ -name "*~" -exec rm -f {} \; -prune
	find ./ -name "__pychache__" -exec rm -rf {} \; -prune
	find ./ -name ".ipynb_checkpoints" -exec rm -rf {} \; -prune
	rm -rf build/
	rm -rf docs/build/
	rm -rf dist/
	rm -rf *.egg-info/

install:
	pipenv install

uninstall:
	pipenv clean
	pipenv --rm