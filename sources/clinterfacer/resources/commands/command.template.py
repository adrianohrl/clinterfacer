#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script was generated by the clinterfacer framework.
It is the main script of the {command} command, which is
available in the {package} command-line interface.
For more details, visit https://gitlab.com/adrianohrl/clinterfacer!
"""

# standard library(ies)
from argparse import Namespace
import logging
import sys
import time

# local source(s)
from {package}.subparsers.{command} import parse_args

logger = logging.getLogger(__name__)


def main(args: Namespace) -> int:
    logger.info('Running the {package}\'s {command} command ...')
    return 0


if __name__ == '__main__':
    start = time.time()
    args = parse_args()
    sys.exit(main(args))
    elapsed_time = (time.time() - start) / 60
    logger.info(f'Elapsed: {{elapsed_time:.2f}} minutes')